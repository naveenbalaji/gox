import React from 'react';
import Bottombars from './components/Bottombars';
import Category from './components/Category'
function App() {
  return (
    <div>
        <Category/>
        <Bottombars />
    </div>
  );
}

export default App;
